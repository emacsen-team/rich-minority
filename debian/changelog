rich-minority (1.0.3-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 26 Jul 2024 11:09:46 +0900

rich-minority (1.0.3-3) unstable; urgency=medium

  * Update my email address.
  * Drop version qualifier for Recommends: emacs, because even oldoldstable
    (stretch) has a new enough version.
  * Declare Standards-Version: 4.6.2 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Sat, 18 Feb 2023 21:58:35 -0500

rich-minority (1.0.3-2) unstable; urgency=medium

  * Rebuild with dh-elpa 2.0.
  * Migrate to debhelper-compat 13.
  * Add Rules-Requires-Root: no.
  * Update my copyright years.
  * Declare Standards-Version: 4.5.1 (no changes required).

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 08 Dec 2020 07:03:38 -0500

rich-minority (1.0.3-1) unstable; urgency=medium

  * New upstream version.
  * Switch to debhelper-compat 12.
  * Drop emacs25 from Enhances, because the package does not exist in Buster.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 08 Jul 2019 17:17:14 -0400

rich-minority (1.0.2-2) unstable; urgency=medium

  * Switch to debhelper-compat 11 and drop debian/compat.
  * Update Maintainer team name and email address.
  * Update Vcs links to use salsa instead of alioth.
  * Update my copyright year range.
  * Declare Standards-Version: 4.3.0. (No additional changes needed)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 08 Feb 2019 23:47:43 -0700

rich-minority (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Fix Vcs-Git URL.
  * Upstream has migrated to GPL-3+.
    - debian/copyright: Updated to reflect change from GPL-2+ to GPL-3+

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 27 Nov 2017 11:31:51 -0500

rich-minority (1.0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #864393)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 29 Sep 2017 23:46:09 -0400
